---
- hosts: webserver

  vars_files:
    - ./vars/vault.yml
    - ./vars/vars.yml

  vars_prompt:
    - name: symfony_env
      prompt: "Enter the environment for your Symfony app (prod|dev|test)"
      default: prod
      private: no

  environment:
    SYMFONY_ENV: "{{ symfony_env|lower }}"

  pre_tasks:
    - name: Convert entered Symfony environment to lowercase
      set_fact:
        symfony_env: "{{ symfony_env|lower }}"
      tags:
        - always

    - name: Update APT package manager repositories cache
      become: true
      apt:
        update_cache: yes

    - name: Upgrade installed packages
      become: true
      apt:
        upgrade: safe

  roles:
    - nginx

    - role: DavidWittman.redis
      become: true

  tasks:
    #- debug:
    #    var: ansible_env
        #msg: '{{ ansible_env }}'

    #- debug:
    #    var: symfony_env
        #msg: '{{ symfony_env }}'

    - ping: ~

    - name: Install low-level utilities
      become: true
      apt:
        name: "{{ item }}"
      with_items:
        - zip
        - unzip

    - name: Install Git VCS
      become: true
      apt:
        name: git
        state: latest

    - name: Install MySQL DB server
      become: true
      apt:
        name: mysql-server
        state: latest

    - name: Add PHP 7 PPA repository
      become: true
      apt_repository:
        repo: 'ppa:ondrej/php'

    - name: Install PHP packages
      become: true
      apt:
        name: "{{ item }}"
        state: latest
      with_items:
        - php7.2-cli
        - php7.2-curl
        - php7.2-fpm
        - php7.2-intl
        - php7.2-mysql
        - php7.2-xml
      notify: Restart PHP-FPM

    - name: Set date.timezone for CLI
      become: true
      lineinfile:
        path: /etc/php/7.2/cli/php.ini
        regexp: "date.timezone ="
        line: "date.timezone = UTC"

    - name: Set date.timezone for FPM
      become: true
      lineinfile:
        path: /etc/php/7.2/fpm/php.ini
        regexp: "date.timezone ="
        line: "date.timezone = UTC"
      notify: Restart PHP-FPM

    - name: Create project directory and set its permissions
      become: true
      file:
        path: "{{ symfony_root_dir }}"
        state: directory
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
        recurse: yes
      tags:
        - deploy

    - name: Checkout Git repository
      git:
        repo: https://github.com/knpuniversity/ansible.git
        dest: "{{ symfony_root_dir }}"
        force: yes
      register: repo_code
      tags:
        - deploy

    - name: Register code_changed variable
      set_fact:
        code_changed: repo_code.changed
      tags:
        - deploy

    - name: Check for Composer
      stat:
        path: /usr/local/bin/composer
      register: composer_stat
      tags:
        - deploy

    # - debug:
    #     var: composer_stat
    #   tags:
    #     - deploy

    - name: Download Composer
      when: not composer_stat.stat.exists
      script: scripts/install_composer.sh
      tags:
        - deploy

    - name: Move Composer globally
      become: true
      when: not composer_stat.stat.exists
      command: mv composer.phar /usr/local/bin/composer
      tags:
        - deploy

    - name: Set permissions on Composer
      become: true
      when: not composer_stat.stat.exists
      file:
        path: /usr/local/bin/composer
        mode: "a+x"
      tags:
        - deploy

    - name: Make sure composer is at its latest version
      when: composer_stat.stat.exists
      composer:
        working_dir: "{{ symfony_root_dir }}"
        command: self-update
      register: composer_self_update
      # for not to make yask changed if Composer is olready on a latest version
      changed_when: "not composer_self_update.stdout is not search('You are already using composer version')"
      tags:
        - deploy

    # Bootstrap Symfony app
    - import_tasks: ./includes/symfony-bootstrap.yml

  handlers:
    - name: Restart PHP-FPM
      become: true
      service:
        name: php7.2-fpm
        state: restarted